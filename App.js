import React, {Component} from 'react';
import {initStore} from './src/redux/store';
import {Provider} from 'react-redux';

import Home from './src/containers/Home';

const store = initStore();

class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <Home />
      </Provider>
    );
  }
}

export default App;